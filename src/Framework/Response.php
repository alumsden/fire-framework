<?php
namespace Lummy\Fire;

class Response{

    public function __construct ($content,$status) {

        self::set($content,$status);
    }

    public static function send($content,$status){
      http_response_code($status);

      if (is_array($content) || is_object($content)) {
        header('Content-Type: application/json');
        echo json_encode($content,TRUE);
        die();
      }

      echo $content;
      die();
    }

    public static function notFound($data =null){

      if (!$data) {
        $data = ['sorry'=>'not founsd'];
      }
      self::send($data,404);
    }
    public static function away($url){
      header('Location: '.$url);
      die();
    }
    public function redirect(){

    }

}


 ?>
