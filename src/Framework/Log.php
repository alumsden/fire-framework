<?php
namespace Lummy\Fire;

class Log{


    public static function info($content){
        self::write($content);
    }

    public static function write($content){

      if (is_array($content) || is_object($content)) {
        $content = json_encode($content,JSON_PRETTY_PRINT);
      }

      $content .= "\n".date('m/d/Y h:i:s a', time())."\n\n".$content;

      file_put_contents(getcwd().'/logs.txt', $content.PHP_EOL , FILE_APPEND | LOCK_EX);
    }

}


 ?>
