<?php
namespace Lummy\Fire;

use Lummy\Fire\Request;
use Lummy\Fire\Response;
use Lummy\Fire\Log;

class Route{

    public static $routes = [];

    public static function add($method,$path,$controller){

        array_push(self::$routes,[
          'method' => $method,
          'path' => $path,
          'controller' => $controller
        ]);

    }

    public static function make(){

      foreach (self::$routes as $route) {

          if($route['method'] !== $_SERVER['REQUEST_METHOD']) {
            continue;
          }

          $success = self::checkStructure($route['path'],$_SERVER['REQUEST_URI']);

          if ($success) {

            // get controller action & controller
            $action = self::getControllerAndAction($route['controller']);

            $controllerName = 'App\\Controllers\\'.$action[0];
            if (!method_exists($controllerName,$action[1])) {
              Response::notFound(['method'=>'not found']);
            }

            $mapped = self::mapVariables($route['path'],$_SERVER['REQUEST_URI']);

            $controller = new $controllerName();
            $response = $controller->{$action[1]}(new Request(),...$mapped);
            Response::send($response,200);
          }

      }

        Response::notFound();

    }
    public static function getControllerAndAction($controller){
        return explode('@',$controller);
    }

    public static function mapVariables($routeUrl, $route){

      list($a, $b) = self::urlToArray($routeUrl, $route);
      $ar = [];
      foreach ($a as $key => $value) {
        if ($value[0] == ':') {


              array_push($ar,$b[$key]);

        }
      }

      return $ar;
    }

    public static function checkStructure($routeUrl,$route){
        list($a, $b) = self::urlToArray($routeUrl, $route);


        // If homepage & has query params
        if ((empty($a) && sizeof($b) == 1) && $b[0][0] == '?') {
          return true;
        }

        if(sizeof($a) !== sizeof($b)){
            return false;
        }


        foreach ($a as $key => $value){

            $replaces = preg_replace('/\?.*/', '',$b[$key]);

            if($value[0] !== ':' && $value !== $replaces){
                return false;
            }
        }

        return true;
    }

    public static function urlToArray($url1, $url2){

     $a = array_values(array_filter(explode('/', $url1), function($val){ return $val !== ''; }));
     $b = array_values(array_filter(explode('/', $url2), function($val){ return $val !== ''; }));
     return array($a, $b);

   }

    public static function get($path,$controller){
        self::add('GET',$path,$controller);
    }

    public static function post($path,$controller){
        self::add('POST',$path,$controller);
    }

    public static function put($path,$controller){
        self::add('PUT',$path,$controller);
    }

    public static function delete($path,$controller){
        self::add('DELETE',$path,$controller);
    }

}

 ?>
